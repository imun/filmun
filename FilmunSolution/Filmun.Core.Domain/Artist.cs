﻿using System.Collections.Generic;
using Filmun.Core.Domain.Base;
namespace Filmun.Core.Domain {
    public class Artist: BaseEntity {

        public Artist() {
            Movies = new HashSet<Movie>();
        }

        #region Properties
        public int Id { get; set; }
        public string Title { get; set; }
        public string AltTitle { get; set; }
        public string Description { get; set; }
        public string PhotoUrl { get; set; }
        #endregion

        #region Navigations
        public virtual ICollection<Movie> Movies { get; set; }

        #endregion
    }
}
