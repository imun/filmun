﻿using System;
using System.Collections.Generic;
using Filmun.Core.Domain.Base;

namespace Filmun.Core.Domain {
    public class Movie: BaseEntity {

        public Movie() {
            Artists = new HashSet<Artist>();
            Genres = new HashSet<Genre>();
            Episodes = new HashSet<Episode>();
            MovieUrls = new HashSet<MovieUrl>();
            Seasons = new HashSet<Season>();
            Subtitles = new HashSet<Subtitle>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string AltTitle { get; set; }
        public string Summary { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string ReleaseDateString { get; set; }
        public string ImdbUrl { get; set; }
        public float ImdbRank { get; set; }
        public string Language { get; set; }
        public int Duration { get; set; }
        public string CoverUrl { get; set; }
        public int Year { get; set; }
        public int? SourceId { get; set; }
        public string SourceUrl { get; set; }
        #region Navigations 
        public virtual Source Source { get; set; }
        public virtual ICollection<Artist> Artists { get; set; }
        public virtual ICollection<Genre> Genres { get; set; }
        public virtual ICollection<Episode> Episodes { get; set; }
        public virtual ICollection<MovieUrl> MovieUrls { get; set; }
        public virtual ICollection<Season> Seasons { get; set; }
        public virtual ICollection<Subtitle> Subtitles { get; set; }
        #endregion
    }
}
