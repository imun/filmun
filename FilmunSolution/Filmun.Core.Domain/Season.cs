﻿using System.Collections.Generic;
using Filmun.Core.Domain.Base;
namespace Filmun.Core.Domain {
    public class Season: BaseEntity {

        public Season() {
            Episodes = new List<Episode>();
        }

        public int Id { get; set; }
        public int OrderNumber { get; set; }
        public int MovieId { get; set; }
        public string Description { get; set; }

        #region Navigations
        public virtual Movie Movie { get; set; }
        public virtual ICollection<Episode> Episodes { get; set; }
        #endregion
    }
}
