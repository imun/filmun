﻿using System;
namespace Filmun.Core.Domain.Base {
    public abstract class BaseEntity {
        public EntityStatus Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
