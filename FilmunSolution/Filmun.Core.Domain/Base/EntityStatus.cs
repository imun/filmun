﻿namespace Filmun.Core.Domain.Base {
    public enum EntityStatus {
        Disabled = 0,
        Enabled = 1,
        Deleted = -1
    }
}
