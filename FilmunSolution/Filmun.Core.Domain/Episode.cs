﻿using System.Collections.Generic;
using Filmun.Core.Domain.Base;
namespace Filmun.Core.Domain {
    public class Episode: BaseEntity {

        public Episode() {
            MovieUrls = new HashSet<MovieUrl>();
            Subtitles = new HashSet<Subtitle>();
        }

        #region Properties
        public int Id { get; set; }
        public string Title { get; set; }
        public int SeasonId { get; set; }
        public int MovieId { get; set; }
        public int Duration { get; set; }
        public string Description { get; set; }
        #endregion

        #region Navigations 
        public virtual Season Season { get; set; }
        public virtual Movie Movie { get; set; }
        public virtual ICollection<MovieUrl> MovieUrls { get; set; }
        public virtual ICollection<Subtitle> Subtitles { get; set; }
        #endregion

    }
}
