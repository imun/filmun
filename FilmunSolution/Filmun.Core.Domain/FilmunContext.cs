﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filmun.Core.Domain {
    public class FilmunContext: DbContext {
        public FilmunContext(): base("FilmunContext") {

        }


        #region Tables
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Episode> Episodes { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieUrl> MovieUrls { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<Source> Sources { get; set; }
        public DbSet<Subtitle> Subtitles { get; set; }
        public DbSet<User> Users { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {

            modelBuilder.Entity<Movie>()
                .HasMany(m => m.Artists)
                .WithMany(a => a.Movies)
                .Map(ma => {
                    ma.MapLeftKey("MovieId");
                    ma.MapRightKey("ArtistId");
                    ma.ToTable("MovieArtists");
                });

            modelBuilder.Entity<Artist>()
                .HasKey(p => p.Id);

            modelBuilder.Entity<Episode>()
                .HasMany(e => e.MovieUrls)
                .WithOptional(mu => mu.Episode);
            modelBuilder.Entity<Episode>()
                .HasMany(e => e.Subtitles)
                .WithOptional(s => s.Episode);

            modelBuilder.Entity<Genre>()
                .HasMany(g => g.Movies)
                .WithMany(m => m.Genres)
                .Map(mg => {
                    mg.MapLeftKey("GenreId");
                    mg.MapRightKey("MovieId");
                    mg.ToTable("MovieGenres");
                });

            modelBuilder.Entity<Movie>() 
                .HasMany(m => m.MovieUrls)
                .WithRequired(mu => mu.Movie)
                .WillCascadeOnDelete();
            modelBuilder.Entity<Movie>()
                .HasMany(m => m.Episodes)
                .WithRequired(e => e.Movie)
                .WillCascadeOnDelete();
            modelBuilder.Entity<Movie>()
                .HasMany(m=> m.Seasons)
                .WithRequired(s=> s.Movie)
                .WillCascadeOnDelete();
            modelBuilder.Entity<Movie>()
                .HasMany(m=> m.Subtitles)
                .WithRequired(s=> s.Movie)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Season>()
                .HasMany(s=> s.Episodes)
                .WithRequired(e=> e.Season)
                .WillCascadeOnDelete();
            
            modelBuilder.Entity<Source>()
                .HasMany(s=> s.Subtitles)
                .WithRequired(su=> su.Source);
            modelBuilder.Entity<Source>()
                .HasMany(s => s.MovieUrls)
                .WithRequired(mu => mu.Source);
            modelBuilder.Entity<Source>()
                .HasMany(s => s.Movies)
                .WithOptional(m => m.Source);

            modelBuilder.Entity<User>()
                .HasMany(u => u.Subtitles)
                .WithOptional(su => su.User);
            modelBuilder.Entity<User>()
                .HasMany(u => u.MovieUrls)
                .WithOptional(mu => mu.User);


            base.OnModelCreating(modelBuilder);
        }
    }
}
