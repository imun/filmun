﻿using System.Collections.Generic;
using Filmun.Core.Domain.Base;

namespace Filmun.Core.Domain {
    public class Source: BaseEntity {

        public Source() {
            MovieUrls = new HashSet<MovieUrl>();
            Subtitles = new HashSet<Subtitle>();
            Movies = new HashSet<Movie>();
        }

        #region Properties
        public int Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        #endregion

        #region Navigations
        public virtual ICollection<MovieUrl> MovieUrls { get; set; }
        public virtual ICollection<Subtitle> Subtitles { get; set; }
        public virtual ICollection<Movie> Movies { get; set; }
        #endregion
    }
}
