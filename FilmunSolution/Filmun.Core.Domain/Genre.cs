﻿using System.Collections.Generic;
using Filmun.Core.Domain.Base;
namespace Filmun.Core.Domain {
    public class Genre: BaseEntity {

        public Genre() {
            Movies = new HashSet<Movie>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Tags { get; set; }
        public string Description { get; set; }
        #region Navigations
        public virtual ICollection<Movie> Movies { get; set; }
        #endregion
    }
}
