﻿using Filmun.Core.Domain.Base;
namespace Filmun.Core.Domain {
    public class Subtitle: BaseEntity {

        public Subtitle() {

        }

        #region Properties
        public int Id { get; set; }
        public string Title { get; set; }
        public int MovieId { get; set; }
        public int? EpisodeId { get; set; }
        public int SourceId { get; set; }
        public int? UserId { get; set; }
        public string Url { get; set; }
        #endregion

        #region Navigations
        public virtual Movie Movie { get; set; }
        public virtual Episode Episode { get; set; }
        public virtual Source Source { get; set; }
        public virtual User User { get; set; }
        #endregion
    }
}
