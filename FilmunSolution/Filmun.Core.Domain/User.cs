﻿using System.Collections.Generic;
using Filmun.Core.Domain.Base;

namespace Filmun.Core.Domain {
    public class User: BaseEntity {

        public User() {
            MovieUrls = new HashSet<MovieUrl>();
            Subtitles = new HashSet<Subtitle>();
        }

        #region Properties
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string TelegramId { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public string Description { get; set; }
        #endregion

        #region Navigations
        public virtual ICollection<MovieUrl> MovieUrls { get; set; }
        public virtual ICollection<Subtitle> Subtitles { get; set; }
        #endregion
    }
}
