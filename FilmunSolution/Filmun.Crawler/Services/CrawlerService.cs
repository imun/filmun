﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Filmun.Core.Domain;
using HtmlAgilityPack;

namespace Filmun.Crawler.Services {
    public class CrawlerService {


        public CrawlerService() {
            Web = new HtmlWeb();
        }

        #region Properties
        public string Url { get; set; }
        public string BaseUrl { get; set; }
        public HtmlDocument Document { get; set; }
        public HtmlDocument Page { get; set; }
        public HtmlWeb Web { get; set; }

        #endregion


        public List<Movie> GetMovies(int? sourceId, string url) {
            var result = new List<Movie>();
            Document = Web.Load(url);
            var articles = Document.DocumentNode
                .SelectNodes("//article");

            foreach (var article in articles.Skip(1)) {
                var movie = new Movie();
                var poster = article.ChildNodes.FirstOrDefault(x => x.HasClass("poster"));
                //movie.CoverUrl = poster.ChildNodes[1].ChildNodes[1].ChildNodes[5].Attributes["src"].Value;
                var header = article.ChildNodes.FirstOrDefault(x => x.Name == "header");
                movie.SourceUrl = header.ChildNodes[1].Attributes["href"].Value;
                movie.Title = header.ChildNodes[1].InnerText.Replace("دانلود فیلم", string.Empty).Trim();
                movie.SourceId = sourceId;
                var footer = article.ChildNodes.FirstOrDefault(x => x.Name == "footer");
                movie.Year = int.Parse(footer.ChildNodes[1].InnerText.Trim());
                var imdb_rank = footer.ChildNodes[3].InnerText.Replace("IMDB : ", string.Empty).Trim();
                float rank;
                if(float.TryParse(imdb_rank, out rank)) {
                    movie.ImdbRank = rank;
                }

                Page = Web.Load(movie.SourceUrl);
                var post = Page.DocumentNode.SelectNodes("//article").FirstOrDefault();
                var post_header = post.ChildNodes.FirstOrDefault(x => x.Name == "header");
                var node_info = post_header.ChildNodes[7].ChildNodes[1].ChildNodes[1].ChildNodes[3];
                var release_date = node_info.ChildNodes[1].InnerText.Trim();
                var genres = node_info.ChildNodes[5].ChildNodes.Where(x=> x.Name == "span");
                var genres_list = new List<Genre>();
                foreach(var g in genres) {
                    genres_list.Add(new Genre {
                        Title = g.InnerText.Trim()
                    });
                }
                movie.CoverUrl = post.ChildNodes[1].ChildNodes[1].ChildNodes[1].Attributes["src"].Value;
                result.Add(movie);
            }

            return result;
        }



    }
}
